# dnstorment

Sends a barrage of should-be-blocked ad domains to a dns server in parallel to test the answer speed and blocking capabilities.

Set up a dns server that blocks the domains found in https://someonewhocares.org/hosts/hosts before running it.