package main

import (
	"bufio"
	"log"
	"net"
	"os"
)

func main() {
	file, err := os.Open("./danpollockhosts.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	concurrency := 50
	sem := make(chan bool, concurrency)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sem <- true
		go func(url string) {
			defer func() { <-sem }()
			log.Println("Requesting domain: " + url)
			net.LookupIP(url)
		}(scanner.Text())
	}
}
