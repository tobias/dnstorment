#!/bin/bash

echo "Loading Dan Pollock's Hostfile"
curl https://someonewhocares.org/hosts/hosts --silent | tail -n +200 | sed 's/127\.0\.0\.1//g' | sed 's/#.*$//g' | sed 's/^ *//; s/ *$//; /^$/d' > ./danpollockhosts.txt

echo "Punching in dns lookups at 50req/s"
go run dnstorment.go

